package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.equals(""))
            return null;
        List<Character> s = statement.replaceAll(" ", "").chars().mapToObj(i -> (char) i).collect(Collectors.toList());
        LinkedList<String> symbols = new LinkedList<>();
        StringBuilder number = new StringBuilder();
        int repeatError = 0;
        int numOfBrackets = 0;
        for (Character i : s) {
            if (i.equals('+') || i.equals('-') || i.equals('*') || i.equals('/')) {
                if (!number.toString().isEmpty()) {
                    try {
                        Double.valueOf(String.valueOf(number));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    symbols.add(number.toString());
                    number = new StringBuilder();
                }
                if (repeatError > 0) {
                    return null;
                } else {
                    repeatError++;
                }
                symbols.add(i.toString());
            } else if (Character.isDigit(i) || i.equals('.')) {
                number.append(i);
                repeatError = 0;
            } else if (i.equals('(')) {
                symbols.add(i.toString());
                numOfBrackets++;

            } else if (i.equals(')')) {
                if (--numOfBrackets < 0 || symbols.getLast().equals("("))
                    return null;
                if (!number.toString().isEmpty()) {
                    try {
                        Double.valueOf(String.valueOf(number));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    symbols.add(number.toString());
                    number = new StringBuilder();
                }
                symbols.add(i.toString());
            } else
                return null;
        }
        if (!number.toString().isEmpty()) {
            try {
                Double.valueOf(String.valueOf(number));
            } catch (NumberFormatException e) {
                return null;
            }
            symbols.add(number.toString());
        }
        if (numOfBrackets > 0)
            return null;
        String answer = round(parceExpr(symbols));
        System.out.println(answer);
        return answer;
    }


    private Double parceExpr(LinkedList<String> expression) {
        System.out.println(expression);
        String currenExpr = expression.removeFirst();
        if (expression.size() == 1) {
            return Double.valueOf(expression.removeFirst());
        }
        if (isOpenBracket.test(currenExpr)) {
            LinkedList<String> templar = new LinkedList<>();
            while (!isCloseBracket.test(expression.getFirst())) {
                templar.add(expression.removeFirst());
            }
            expression.removeFirst();
            expression.addFirst(String.valueOf(parceExpr(templar)));
        } else if (isMinus.test(currenExpr)) {
            System.out.println("here");
            Double t = -Double.valueOf(String.valueOf(expression.removeFirst()));
            expression.addFirst(String.valueOf(t));
        } else {
            String nextExpression = expression.removeFirst();
            if (isOpenBracket.test(expression.getFirst())) {
                LinkedList<String> templar = new LinkedList<>();
                expression.removeFirst();
                while (!isCloseBracket.test(expression.getFirst())) {
                    templar.add(expression.removeFirst());
                }
                expression.removeFirst();
                System.out.println(templar);
                expression.addFirst(String.valueOf(parceExpr(templar)));
            }
            if (isMultiplication.test(nextExpression)) {
                Double t = Double.valueOf(String.valueOf(currenExpr))
                        * Double.valueOf(String.valueOf((expression.removeFirst())));
                expression.addFirst(String.valueOf(t));
            }
            if (isDivision.test(nextExpression)) {
                Double p = Double.valueOf(String.valueOf((expression.removeFirst())));
                if (p == 0)
                    return null;
                Double t = Double.valueOf(String.valueOf(currenExpr)) / p;
                expression.addFirst(String.valueOf(t));
            }
            if (isPlus.test(nextExpression)) {
                Double t;
                if (expression.size() == 1 || isMinus.test(expression.get(1)) || isPlus.test(expression.get(1))) {
                    t = Double.valueOf(String.valueOf(currenExpr))
                            + Double.valueOf(expression.removeFirst());
                } else {
                    t = Double.valueOf(String.valueOf(currenExpr))
                            + Double.valueOf(String.valueOf(parceExpr(expression)));
                }
                expression.addFirst(String.valueOf(t));
            }
            if (isMinus.test(nextExpression)) {
                Double t;
                if (expression.size() == 1 || isMinus.test(expression.get(1)) || isPlus.test(expression.get(1))) {
                    t = Double.valueOf(String.valueOf(currenExpr))
                            - Double.valueOf(expression.removeFirst());
                } else {
                    t = Double.valueOf(String.valueOf(currenExpr))
                            - Double.valueOf(String.valueOf(parceExpr(expression)));
                }
                expression.addFirst(String.valueOf(t));
            }
        }
        if (expression.size() == 1) {
            return Double.valueOf(expression.removeFirst());
        } else {
            return parceExpr(expression);
        }
    }

    private Predicate<String> isDigit = s -> s.length() > 1 || s.matches("[0-9]");
    private Predicate<String> isOpenBracket = s -> s.equals("(");
    private Predicate<String> isCloseBracket = s -> s.equals(")");
    private Predicate<String> isMinus = s -> s.equals("-");
    private Predicate<String> isPlus = s -> s.equals("+");
    private Predicate<String> isDivision = s -> s.equals("/");
    private Predicate<String> isMultiplication = s -> s.equals("*");

    private String round(Double f) {
        if (f == null)
            return null;
        if (f.intValue() == f) {
            return String.valueOf(f.intValue());
        } else {
            f = Math.ceil(f * 10000);
            return String.valueOf(f / 10000);
        }
    }
    
}
