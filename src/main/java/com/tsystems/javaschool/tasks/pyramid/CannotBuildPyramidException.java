package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {

    public CannotBuildPyramidException() {
        super();
    }

    public CannotBuildPyramidException(String s) {
        super(s);
    }
}
