package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        checkOnNull(inputNumbers);
        int levelNumber = getLevelNumber(inputNumbers.size());
        Collections.sort(inputNumbers);
        int levelLength = 2 * levelNumber - 1;
        int[][] result = new int[levelNumber][levelLength];
        int currentIndex = inputNumbers.size() - 1;
        for (int i = levelNumber - 1; i >= 0; i--) {
            int startIndex = levelNumber - 1 + i;
            int endIndex = levelNumber - 1 - i;
            for (int j = levelLength - 1; j >= 0; j--) {
                if (j == startIndex && currentIndex >= 0) {
                    result[i][j] = inputNumbers.get(currentIndex);
                    currentIndex--;
                    if (startIndex > endIndex)
                        startIndex = startIndex - 2;
                } else {
                    result[i][j] = 0;
                }
            }
        }
        return result;
    }

    private void checkOnNull(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException("Contains null");
    }

    private Integer getLevelNumber(int length) {
        int levelNumber = 1;
        while (length > 0) {
            length = length - levelNumber;
            levelNumber++;
        }
        if (length == 0 || length == -1) {
            return levelNumber - 1;
        } else {
            throw new CannotBuildPyramidException("Unacceptable length of list");
        }
    }


}
