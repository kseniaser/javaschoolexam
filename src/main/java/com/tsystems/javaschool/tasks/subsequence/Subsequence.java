package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        checkOnNull(x, y);
        if (!x.isEmpty() && y.isEmpty())
            return false;
        int startIndex = 0;
        for (Object t : x) {
            while (!t.equals(y.get(startIndex))) {
                if (++startIndex >= y.size())
                    return false;
            }
        }
        return true;
    }

    private void checkOnNull(List x, List y) {
        if (y == null || x == null)
            throw new IllegalArgumentException();
    }
}
